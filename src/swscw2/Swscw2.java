/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swscw2;
//import com.hp.hpl.jena.query.*;
//import com.hp.hpl.jena.rdf.model.impl.ResourceImpl;

import java.io.IOException;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.impl.ResourceImpl;
import org.apache.jena.riot.Lang;
import org.apache.jena.sparql.*;
import org.apache.jena.sparql.engine.http.*;


//import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;


/**
 *
 * @author Bibi
 */
public class Swscw2 {

  public static void main(String[] args) throws IOException {
        Model m = ModelFactory.createDefaultModel() ;
        // read into the model.
        String current = new java.io.File( "." ).getCanonicalPath() + "/src/input/dataSWS1.ttl";
        m.read(current);
        
//        //query1: for every slot in the (perhaps) exhibition, state the ID of the 
//        //slot and the number of females and males exhibited in the slot
//        
//        //helping source: https://docs.oracle.com/cd/NOSQL/html/RDFGraph/example5.html
//        String q1  = "" +
//                "PREFIX ORACLE_SEM_FS_NS: " +
//                " <http://oracle.com/semtech#join_method=nl> \n" +     
//                "prefix olo: <http://purl.org/ontology/olo/core#>\n " + 
//                "prefix dbo: <http://dbpedia.org/ontology/>\n" + 
//                "prefix foaf: <http://xmlns.com/foaf/0.1/>\n" +
//                "select ?slotNumber ?numberOfMen ?numberOfWomen { \n " +
//                "{select ?slotNumber (count (?men) as ?numberOfMen)\n " +
//                "where {\n" + 
//                "?men foaf:gender \"male\" . \n " +
//                "?men olo:slot ?slotNumber . \n " + 
//              "} group by ?slotNumber}\n" + 
//                "{select ?slotNumber (count (?women) as ?numberOfWomen) \n " +
//                "where {\n" + 
//                "?women foaf:gender \"female\" . \n" + 
//                "?women olo:slot ?slotNumber . \n" + 
//              "} group by ?slotNumber}" + 
//                "}" ; 
//                
//        QueryExecution qe1 = QueryExecutionFactory.create(q1, m);
//        ResultSet results1 = qe1.execSelect();
//        System.out.println("-----------------------------------\n RESULTS Q1");
//        while(results1.hasNext()){
//            System.out.println(results1.next().toString());
//        }
//           
//        //query 2 : find the names of all female members of parliament who were also 
//        //associated with religion and not related to England
//        String q2 = ""+
//            "prefix dbo: <http://dbpedia.org/ontology/>\n" + 
//            "prefix dbpedia-owl: <http://dbpedia.org/ontology/>\n" + 
//            "prefix prov: <http://www.w3.org/ns/prov#>\n" +    
//            "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n" +
//            "prefix foaf: <http://xmlns.com/foaf/0.1/>\n" +
//            "select ?name \n" + 
//            "where {\n" + 
//            "?person foaf:name  ?name . \n " +
//            "?person a dbo:MemberOfParliament . \n"  +
//            "?person foaf:gender \"female\" .\n" +
//            "?person prov:wasAssociatedWith dbo:religion . \n" + 
//            "MINUS {?person ?relation <http://dbpedia.org/resource/England> .} \n" +
//            "}";
//        
//        QueryExecution qe2 = QueryExecutionFactory.create(q2, Syntax.syntaxARQ, m);
//        ResultSet results2 = qe2.execSelect();
//        System.out.println("-----------------------------------\n RESULTS Q2");
//        while(results2.hasNext()){
//            System.out.println(results2.next().toString());
//        }
//        
//        //query 3 : Find occupations/professions/associations by counts
//        String q3 = "" + 
//              "prefix dbo: <http://dbpedia.org/ontology/>\n" + 
//            "prefix dbpedia-owl: <http://dbpedia.org/ontology/#> \n " + 
//            "prefix foaf: <http://xmlns.com/foaf/0.1/>\n" +
//            "prefix prov: <http://www.w3.org/ns/prov#>\n" + 
//            "select ?prof (count (?person1) as ?nopeople) \n" + 
//            "where {\n" + 
//            "{?person1 a ?prof .} UNION"   + 
//            "{?person1 prov:wasAssociatedWith ?prof .} UNION" + 
//            "{?person1 dbpedia-owl:occupation ?prof .} \n" +
//            "} \n" + 
//            "group by ?prof \n" + 
//            "limit 10";   
//        
//        QueryExecution qe3 = QueryExecutionFactory.create(q3, Syntax.syntaxARQ, m);
//        ResultSet results3 = qe3.execSelect();
//        System.out.println("-----------------------------------\n RESULTS Q3");
//        while(results3.hasNext()){
//            System.out.println(results3.next().toString());
//        }
//        
        //federated query: find the birth years of people who can be found on DBPedia
                String q4  = "" +
                "prefix dbo: <http://dbpedia.org/ontology/>\n" + 
                "prefix dbp: <http://dbpedia.org/ontology/>\n"+
                "prefix dbpedia-owl: <http://dbpedia.org/ontology/#>  \n"+
                "select distinct ?placeRes ?pageId ?cap ?capId \n " +
                "where {\n"+
                "?person dbpedia-owl:birthPlace ?placeRes . \n" +
                "service <http://dbpedia.org/sparql> {\n"+
                  "?placeRes dbo:wikiPageID ?pageId . \n " +
                  "?placeRes dbp:capital ?cap . \n" +
                  "?cap dbo:wikiPageID ?capId . \n" +
                "}\n"+
                "}limit 10";
                        
        QueryExecution qe4 = QueryExecutionFactory.create(q4, Syntax.syntaxARQ, m);
        ResultSet results4 = qe4.execSelect();
        System.out.println("-----------------------------------\n RESULTS Q4");
        while(results4.hasNext()){
            System.out.println(results4.next().toString());
        }
  }
}
